const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

const db = require("./queries");

// serve the static files from react
app.use(express.static(path.join(__dirname, "build")));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (request, response) => {
    response.json({ info: "Price Register API" });
});

app.get("/products", db.getProducts);
app.get("/products/:id", db.getProductById);
app.post("/users", db.createProduct);
app.put("/users/:id", db.updateProduct);
app.delete("/users/:id", db.deleteProduct);

// handle any requests that don't match the api endpoints
app.get("*", (request, response) => {
    response.sendFile(path.join(__dirname + "/build/index.html"));
});

app.listen(port, () => {
    console.log(`App running on port ${port}.`);
});
