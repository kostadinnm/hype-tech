const { Pool } = require("pg");

const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "pricereg",
    password: "*****",
    port: 5432
});

const getProducts = (request, response) => {
    pool.query("SELECT * FROM products ORDER BY id ASC", (err, res) => {
        if (err) {
            throw err;
        }
        response.status(200).json(res.rows);
    });
};

const getProductById = (request, response) => {
    const id = parseInt(request.params.id);
    pool.query("SELECT * FROM products WHERE id = $1", [id], (err, res) => {
        if (err) {
            throw err;
        }
        response.status(200).json(res.rows);
    });
};

const createProduct = (request, response) => {
    const { name, description, price, barcode } = request.body;
    pool.query(
        "INSERT INTO products (name, description, price, barcode) VALUES ($1, $2, $3, $4)",
        [name, description, price, barcode],
        (err, res) => {
            if (err) {
                throw err;
            }
            response.status(201).send(`Product added with ID: ${res.insertId}`);
        }
    );
};

const updateProduct = (request, response) => {
    const id = parseInt(request.params.id);
    const { name, desciption, price, barcode } = request.body;
    pool.query(
        "UPDATE products SET name=$1, description=$2, price=$3, barcode=$4",
        [name, desciption, price, barcode],
        (err, res) => {
            if (err) {
                throw err;
            }
            response.status(200).send(`Product modified with ID: ${id}`);
        }
    );
};

const deleteProduct = (request, response) => {
    const id = parseInt(request.params.id);
    pool.query("DELETE FROM products WHERE id = $1", [id], (err, res) => {
        if (err) {
            throw err;
        }
        response.status(200).send(`Product deleted with ID: ${id}`);
    });
};

module.exports = {
    getProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
};
