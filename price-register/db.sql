#psql -h <host - local or remote> -d postgres -U postgres

CREATE DATABASE pricereg;
CREATE TABLE products (
    ID SERIAL PRIMARY KEY,
    name VARCHAR(30),
    description VARCHAR(30),
    price VARCHAR(30),
    barcode VARCHAR(30)
);